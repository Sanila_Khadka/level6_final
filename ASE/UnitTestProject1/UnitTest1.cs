using Microsoft.VisualStudio.TestTools.UnitTesting;
using Software;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Rectangle re = new Rectangle();

            int x = 200, y = 200, width = 100, height = 100;
            re.set(x, y, width, height);
            Assert.AreEqual(200, re.x);
        }

        [TestMethod]
        public void TestMethod2()
        {
            Circle rec = new Circle();

            int x = 200, y = 100, radius = 0;
            rec.set(x, y, radius);
            Assert.AreEqual(00, rec.radius);
        }
    }
}
