﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software
{
    public interface IShape
    {
        /// <summary>
        /// creating graphics object g
        /// </summary>
        /// <param name="g">interface method</param>
        void draw(Graphics g);
        /// <summary>
       
        /// </summary>
        /// <param name="list"></param>
        void set(params int[] list);
    }
}
