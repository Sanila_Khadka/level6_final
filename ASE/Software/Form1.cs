﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Software
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Inisilizaing another variable
        /// </summary>
        int loop = 0, kStart = 0, ifcounter = 0;
        private bool loopcheck;
        Creator factory = new Factory();
        Pen myPen = new Pen(Color.Red);
        int x = 0, y = 0, width, height, radius, point, repeatval, counter;

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            panel1.Refresh();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This is a made by Sanila Khadka");
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text File (.txt)| *.txt";
            saveFileDialog.Title = "Save File...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = new StreamWriter(saveFileDialog.FileName);
                fWriter.Write(textBox1.Text);
                fWriter.Close();
            }
            textBox1.Text += "Command Save";
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog loadFileDialog = new OpenFileDialog();
            loadFileDialog.Filter = "Text File (.txt)|*.txt";
            loadFileDialog.Title = "Open File...";

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = new System.IO.StreamReader(loadFileDialog.FileName);
                textBox1.Text = streamReader.ReadToEnd();
                streamReader.Close();
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Run_Click(object sender, EventArgs e)
        {
            Graphics g = panel1.CreateGraphics();

            string command = textBox1.Text.ToLower();
            string[] commandline = command.Split(new String[] { "\n" },
                StringSplitOptions.RemoveEmptyEntries);
            
            for (int k = 0; k < commandline.Length; k++)
            {
                textBox2.Text = textBox2.Text.ToLower();
                string[] cmd = commandline[k].Split(' ');
                if (textBox2.Text.Equals("run") == true)
                {


                    if (textBox1.Text.Equals("clear") == true)
                    {
                        textBox1.Text = "";
                        textBox2.Text = "";
                        panel1.Refresh();

                    }
                    else if (textBox1.Text.Equals("Exit") == true)
                    {
                        this.Close();
                    }
                    else if (textBox1.Text.Equals("Reset") == true)
                    {
                        x = 0;
                        y = 0;
                        width = 0;
                        height = 0;
                        textBox1.Text = "";
                        textBox2.Text = "";
                        panel1.Refresh();
                    }
                    else if (cmd[0].Equals("moveto") == true)
                    {
                        panel1.Refresh();
                        string[] param = cmd[1].Split(',');
                        if (param.Length != 2)
                        { MessageBox.Show("Please correct something"); }
                        else
                        {
                            Int32.TryParse(param[0], out x);
                            Int32.TryParse(param[1], out y);
                            moveTo(x, y);
                        }

                    }
                    else if (cmd[0].Equals("radius") == true)
                    {
                        int r;
                        if (cmd[1].Equals("=") == true)
                        {
                            Int32.TryParse(cmd[2], out radius);
                        }
                        else if (cmd[1].Equals("+") == true)
                        {
                            Int32.TryParse(cmd[2], out r);
                            radius = radius + r;
                        }
                        else if (cmd[1].Equals("-") == true)
                        {
                            for (int rc = 0; rc < repeatval; rc++)
                            {
                                Int32.TryParse(cmd[2], out r);
                                radius = radius - r;
                            }
                        }
                        else { MessageBox.Show("Syntax Error"); }
                    }
                    else if (cmd[0].Equals("width") == true)
                    {
                        int w;
                        if (cmd[1].Equals("=") == true)
                        {
                            Int32.TryParse(cmd[2], out width);
                        }
                        else if (cmd[1].Equals("+") == true)
                        {
                            Int32.TryParse(cmd[2], out w);
                            width = width + w;
                        }
                        else if (cmd[1].Equals("-") == true)
                        {
                            for (int rc = 0; rc < repeatval; rc++)
                            {
                                Int32.TryParse(cmd[2], out w);
                                width = width - w;
                            }
                        }

                        else
                        {
                            MessageBox.Show("Syntax Error");
                        }
                    }
                    else if (cmd[0].Equals("height") == true)
                    {
                        int h;
                        if (cmd[1].Equals("=") == true)
                        {
                            Int32.TryParse(cmd[2], out height);
                        }
                        else if (cmd[1].Equals("+") == true)
                        {
                            Int32.TryParse(cmd[2], out h);
                            height = height + h;
                        }
                        else if (cmd[1].Equals("-") == true)
                        {
                            for (int rc = 0; rc < repeatval; rc++)
                            {
                                Int32.TryParse(cmd[2], out h);
                                height = height - h;

                            }
                        }
                        else
                        {
                            MessageBox.Show("Syntax Error");
                        }
                    }
                    else if (cmd[0].Equals("drawto") == true)
                    {
                        string[] param = cmd[1].Split(',');
                        int x = 0, y = 0;
                        if (param.Length != 2)
                        {
                            MessageBox.Show("Incorrect Parameter");
                        }
                        else
                        {
                            Int32.TryParse(param[0], out x);
                            Int32.TryParse(param[1], out y);
                            drawTo(x, y);
                        }
                    }
                    else if (cmd[0].Equals("drawline") == true)
                    {
                        string[] param = cmd[1].Split(',');
                        int toX = 0, toY = 0;
                        if (param.Length != 2)
                        {
                            MessageBox.Show("Incorrect parameter please use both width and height");
                        }
                        else
                        {
                            Int32.TryParse(param[0], out toX);
                            Int32.TryParse(param[1], out toY);
                            IShape line = factory.getShape("line");
                            line.set(x, y, toX, toY);
                            line.draw(g);
                        }
                    }
                    else if (cmd[0].Equals("circle") == true)
                    {
                        if (cmd.Length != 2) { MessageBox.Show("Incorrect Parameter only radius"); }

                        {
                            if (cmd[1].Equals("radius") == true)
                            {
                                IShape circle = factory.getShape("circle");
                                Circle c = new Circle();
                                c.set(x, y, radius);
                                c.draw(g);
                            }
                            else
                            {
                                Int32.TryParse(cmd[1], out radius);
                                IShape circle = factory.getShape("circle");
                                Circle c = new Circle();
                                c.set(x, y, radius);
                                c.draw(g);
                            }
                        }
                    }

                    else if (cmd[0].Equals("rectangle") == true)
                    {
                        if (cmd.Length != 2)
                        {
                            MessageBox.Show("Invalid Parameter please use the value in x and y");
                        }
                        else
                        {
                            string[] param = cmd[1].Split(',');
                            if (param.Length != 2)
                            {
                                MessageBox.Show("Invalid Parameter 2 Side only");
                            }
                            else
                            {
                                Int32.TryParse(param[0], out width);
                                Int32.TryParse(param[1], out height);
                                IShape circle = factory.getShape("rectangle");
                                Rectangle r = new Rectangle();
                                r.set(x, y, width, height);
                                r.draw(g);
                            }
                        }
                    }

                    else if (cmd[0].Equals("triangle") == true)
                    {
                        string[] param = cmd[1].Split(',');
                        if (param.Length != 3)
                        {
                            MessageBox.Show("Incorrect Parameter");

                        }
                        else
                        {
                            Int32.TryParse(param[0], out width);
                            Int32.TryParse(param[1], out height);
                            IShape circle = factory.getShape("triangle");
                            Triangle r = new Triangle();
                            r.set(x, y, width, height);
                            r.draw(g);
                        }
                    }
                   
                    else if (!cmd[0].Equals(null))
                    {
                        int errorLine = k + 1;
                        MessageBox.Show("Invalid syntax Found on line " + errorLine, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                {
                    MessageBox.Show("Please Type 'Run'  Command to get the output", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="toX"> This method is used to change the tox  inside a point obect</param>
        /// <param name="toY">This method is used to change the toy  data inside a point object</param>
        public void moveTo(int toX, int toY)
        { x = toX; y = toY; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="toX">This method is used to change the tox  inside a point obect</param>
        /// <param name="toY">This method is used to change the toy  data inside a point object</param>
        public void drawTo(int toX, int toY)
        { x = toX; y = toY; }
    }
}