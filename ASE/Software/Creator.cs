﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software
{
    abstract class Creator

    {
        /// <summary>
        /// this is used as passing the shape of object.
        /// </summary>
        /// <param name="ShapeType"> shape of the obect</param>
        /// <returns></returns>
        public abstract IShape getShape(string ShapeType);



    }
}
