﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software
{
    public class Rectangle : IShape
    {

        /// <summary>
        /// declaring the x, y, width, height variable in integer datatype.
        /// </summary>
        public int x, y, width, height;

        /// <summary>
        /// provides width and height of rectangle
        /// </summary>
        public Rectangle() : base()
        {
            width = 0;
            height = 0;
        }
        /// <summary>
        ///  this used to refer to the current instance of the class.
        /// </summary>
        /// <param name="x">x-axis</param>
        /// <param name="y">y-axis</param>
        /// <param name="width">rectangle's width</param>
        /// <param name="height">rectangle's height</param>

        public Rectangle(int x, int y, int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// create graphics object g
        /// </summary>
        /// <param name="g"> graphic object</param>
        public void draw(Graphics g)
        {
            try
            {
                Pen p = new Pen(Color.Black, 2); // black pen 2 pixels wide
                // call the DrawPolygon() method of the graphics object
                g.DrawRectangle(p, x - (width / 2), y - (height / 2), width * 2, height * 2);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="list">array of an integer</param>
        public void set(params int[] list)
        {
            try
            {
                this.x = list[0];
                this.y = list[1];
                this.width = list[2];
                this.height = list[3];
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
