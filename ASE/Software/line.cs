﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Software
{
    public class Line : IShape
    {
        /// <summary>
        /// pass interger values X as x-axis and Y as y-axis 
        /// </summary>
        public int x, y, toX, toY;


        public Line() : base()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="toX"></param>
        /// <param name="toY"></param>
        public Line(int x, int y, int toX, int toY)
        { }


        /// <summary>
        /// create graphics object g
        /// </summary>
        /// <param name="g">Graphics</param>
        public void draw(Graphics g)
        {
            try
            {
                Pen mypen = new Pen(Color.Black, 2);//create the mypen object 
                g.DrawLine(mypen, x, y, toX, toY);// call the DrawLine() method of the graphics object

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        ///  setting the value of x and y of line x-axis and y-axis.
        /// </summary>
        /// <param name="list"></param>
        public void set(params int[] list)
        {
            this.x = list[0];
            this.y = list[1];
            this.toX = list[2];
            this.toY = list[3];
        }
    }
}
